from tink_monitor.store import StampModel, add
import random
from datetime import datetime, timedelta

datetime.now().date().replace(month=7, day=4)

for i in range(500):
    date = datetime.now() - timedelta(days=random.randint(0, 7), seconds=random.randint(0, 3600))
    print(date)
    val = round(random.uniform(20, 50), 2)
    add(val, date, check_latest=False)

from tink_monitor import store
rec = store.get_week_values()
for r in rec:
    print(r['date'])
len(rec)