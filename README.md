
## INSTALL

Required Python 3.9+

```shell
scripts/install.cmd
```

## START

Replace `python` to `pythonw` to hide console 

```shell
scripts/start.cmd
```

