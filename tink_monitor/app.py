import traceback
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from pathlib import Path
from datetime import datetime
from .ui.utils import set_style
from .ui import widget
from .config import read
from .store import add
from .parsers import us_tink
from .alerts import Alert
import time

icon = Path(__file__).parent / 'img/app.ico'


class MainApp(QObject):
    rightClickSignal = Signal(object)
    leftClickSignal = Signal(object)
    middleClickSignal = Signal(object)
    doubleClickSignal = Signal(object)

    def __init__(self):
        super().__init__()
        self.tray_icon = QSystemTrayIcon()
        self.tray_icon.setIcon(QIcon(icon.as_posix()))
        self.tray_icon.activated.connect(self.tray_icon_activated)
        self.rightClickSignal.connect(self.open_menu)
        self.leftClickSignal.connect(self.activate_widget)

        self.widget = widget.CourseWidget()
        self.widget.onClose.connect(self.on_close)
        self.widget.reload()
        self.widget.show()

        self.alert = Alert()
        self.widget.check_alarm_btn.clicked.connect(self.alert.check_values)
        self.widget.config_changed.connect(self.alert.reset_prev)

        self.th = QThread()
        self.w = Worker()
        self.w.moveToThread(self.th)
        self.th.started.connect(self.w.start)
        self.w.updated.connect(self.reload)
        self.w.finished.connect(self.th.quit)
        self.th.finished.connect(self.on_stopped)
        self.th.start()
        self.reload()

    def on_stopped(self):
        QApplication.instance().quit()

    def reload(self):
        self.widget.reload_data()
        self.alert.check_values()

    def show(self):
        self.tray_icon.show()

    def tray_icon_activated(self, reason):
        """
        Execute click on tray icon if is WINDOWS
        """
        # QSystemTrayIcon.Trigger       LMB
        # QSystemTrayIcon.Context       RMB
        # QSystemTrayIcon.MiddleClick   MMB
        # QSystemTrayIcon.DoubleClick   DBC
        pos = self.tray_icon.geometry().center()
        if reason == QSystemTrayIcon.Context:
            self.rightClickSignal.emit(pos)
        elif reason == QSystemTrayIcon.Trigger:
            self.leftClickSignal.emit(pos)
        elif reason == QSystemTrayIcon.MiddleClick:
            self.middleClickSignal.emit(pos)
        elif reason == QSystemTrayIcon.DoubleClick:
            self.doubleClickSignal.emit(pos)

    def on_close(self):
        self.tray_icon.hide()
        self.widget.hide()
        if self.th.isRunning():
            self.w.stop()
        else:
            self.on_stopped()

    def open_menu(self, pos):
        menu = QMenu()
        set_style(menu)
        menu.addAction(QAction('Options', menu, triggered=self.widget.open_config_dialog))
        menu.addAction(QAction('Exit', menu, triggered=self.on_close))
        menu.exec(pos)

    def activate_widget(self):
        self.widget.activateWindow()


class Worker(QObject):
    finished = Signal()
    updated = Signal()
    error = Signal(str)

    def __init__(self):
        super().__init__()
        self.stopped = False
        self.delay = 5*60

    def stop(self):
        self.stopped = True

    def get_delay(self):
        conf = read()
        interval = max(60, conf.get('update_interval', 60*5))
        return interval

    def start(self):
        while True:
            try:
                data = us_tink.parse()
                # data = self._fake_parse()
                add(data['buy'], data['sell'])
            except Exception as e:
                traceback.print_exc()
                self.error.emit(str(e))
            else:
                self.updated.emit()
            delay = self.get_delay()
            for i in range(delay):
                if self.stopped:
                    break
                time.sleep(1)
            if self.stopped:
                break
        self.finished.emit()

    def _fake_parse(self):
        import random
        return {'buy': round(random.uniform(50, 60), 2), 'sell': round(random.uniform(60, 70), 2)}