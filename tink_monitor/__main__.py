from PySide6.QtWidgets import QApplication

from tink_monitor.app import MainApp

qApp = QApplication([])
window = MainApp()
window.show()
qApp.setQuitOnLastWindowClosed(True)
qApp.exec()
