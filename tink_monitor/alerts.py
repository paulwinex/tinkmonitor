from .config import read
from . import store


class Alert:
    def __init__(self):
        self.prev_value_buy = 0
        self.prev_value_sell = 0

    def reset_prev(self):
        self.prev_value_buy = self.prev_value_sell = 0

    def check_values(self):
        conf = read()
        current = store.get_current_value()
        messages = []
        # sell
        current_sell_cost = current['buy']
        if conf.get('sell_above_alarm', False):
            a_s = conf.get('above_sell', 0)
            if a_s != 0:
                if current_sell_cost > a_s:
                    if current_sell_cost != self.prev_value_sell:
                        messages.append(f'Selling above ${a_s}\n▫️<b>${current_sell_cost}</b>\n')
        if conf.get('sell_below_alarm', False):
            b_s = conf.get('below_sell', 0)
            if b_s != 0:
                if current_sell_cost < b_s:
                    if current_sell_cost != self.prev_value_sell:
                        messages.append(f'Selling below ${b_s}\n▫️<b>${current_sell_cost}</b>\n')
        self.prev_value_sell = current_sell_cost
        # buy
        current_buy_cost = current['sell']
        if conf.get('buy_above_alarm', False):
            a_b = conf.get('above_buy', 0)
            if a_b != 0:
                if current_buy_cost > a_b:
                    if current_buy_cost != self.prev_value_buy:
                        messages.append(f'Purchase above ${a_b}\n▫️<b>${current_buy_cost}</b>\n')
        if conf.get('buy_below_alarm', False):
            b_b = conf.get('below_buy', 0)
            if b_b != 0:
                if current_buy_cost < b_b:
                    if current_buy_cost != self.prev_value_buy:
                        messages.append(f'Purchase below ${b_b}\n▫️<b>${current_buy_cost}</b>\n')
        self.prev_value_buy = current_buy_cost
        if messages:
            self.show_alerts(messages)

    def show_alerts(self, messages: list):
        send_message('\n'.join(messages))


def send_message(text):
    import requests
    conf = read()
    token = conf.get('tg_token')
    if not token:
        return
    chat_id = conf.get('chat_id')
    if not chat_id:
        return
    response = requests.post(
        url='https://api.telegram.org/bot{0}/sendMessage'.format(token),
        data={'chat_id': chat_id, 'text': text, 'parse_mode': 'html'}
    ).json()
    print(response)