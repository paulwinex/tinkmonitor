from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
from pathlib import Path
import logging
from datetime import datetime, timedelta
from collections import defaultdict

logging.getLogger('peewee').disabled = True

DB_FILE: Path = Path.home() / '.tink_monitor.db'
db = SqliteExtDatabase(DB_FILE)


class BaseModel(Model):
    class Meta:
        database = db
    id = PrimaryKeyField()


class StampModel(BaseModel):
    date = DateTimeField(null=True)
    buy = FloatField()
    sell = FloatField()

    def __repr__(self):
        return f'[{self.date} : {self.buy}/{self.sell}]'


if not DB_FILE.exists():
    DB_FILE.parent.mkdir(parents=True, exist_ok=True)
    StampModel.create_table()


def add(value_buy: float, value_sell: float, date: datetime = None, check_latest=True) -> StampModel:
    date = date or datetime.now()
    if check_latest:
        latest = get_latest()
        if latest:
            if date < latest.date + timedelta(minutes=5):
                return
            if latest.buy == value_buy and latest.sell == value_sell:
                return
    return StampModel.create(buy=round(value_buy, 2),
                             sell=round(value_sell, 2),
                             date=date or datetime.now())


def get_range(from_date: datetime, to_date: datetime) -> tuple:
    return StampModel.select().where(StampModel.date.between(from_date, to_date)).order_by(StampModel.date)


def get_latest():
    try:
        return StampModel.select().order_by(StampModel.date.desc()).get()
    except Exception as e:
        print(e)


# def get_range_type(range_type: int):
#     """
#     "Week": 1,
#     "3 Days": 2,
#     "1 Day": 3,
#     "12 Hours": 4,
#     "6 Hours": 5,
#     "3 Hours": 6,
#     "1 Hour": 7
#     """
#     to_date = datetime.now()
#     if range_type == 1:
#         from_date = datetime.now() - timedelta(weeks=1)
#     elif range_type == 2:
#         from_date = datetime.now() - timedelta(days=3)
#     elif range_type == 3:
#         from_date = datetime.now() - timedelta(days=1)
#     elif range_type == 4:
#         from_date = datetime.now() - timedelta(hours=12)
#     elif range_type == 5:
#         from_date = datetime.now() - timedelta(hours=3)
#     elif range_type == 5:
#         from_date = datetime.now() - timedelta(hours=1)
#     else:
#         from_date = datetime.now() - timedelta(days=1)
#     return get_range(from_date, to_date)


# def get_min_max(qs):
#     if qs.count():
#         max_buy = qs.select(fn.MAX(StampModel.buy)).scalar()
#         min_buy = qs.select(fn.MIN(StampModel.buy)).scalar()
#         max_sell = qs.select(fn.MAX(StampModel.sell)).scalar()
#         min_sell = qs.select(fn.MIN(StampModel.sell)).scalar()
#
#         return dict(
#             max_buy=max_buy,
#             min_buy=min_buy,
#             max_sell=max_sell,
#             min_sell=min_sell
#         )
#     else:
#         return dict(
#             max_buy=0,
#             min_buy=0,
#             max_sell=0,
#             min_sell=0
#         )

def get_current_value():
    latest = get_latest()
    if not latest:
        return
    return {'buy': latest.buy, 'sell': latest.sell}


def get_week_max():
    week_ago = datetime.now() - timedelta(weeks=1)
    return {'sell': StampModel.select(fn.MAX(StampModel.sell)).where(StampModel.date > week_ago).scalar(),
            'buy': StampModel.select(fn.MAX(StampModel.buy)).where(StampModel.date > week_ago).scalar()}


def get_today_max():
    day_ago = datetime.now() - timedelta(days=1)
    # today = datetime.now().day
    # qs = StampModel.select().where(StampModel.date > from_date).order_by(StampModel.date)
    sell_val = StampModel.select(fn.MAX(StampModel.sell)).where(StampModel.date > day_ago).scalar()
    buy_val = StampModel.select(fn.MAX(StampModel.buy)).where(StampModel.date > day_ago).scalar()
    return {'buy': buy_val, 'sell': sell_val}


def get_week_values():
    week_ago = datetime.now() - timedelta(weeks=1)
    qs = StampModel.select().where(StampModel.date > week_ago, StampModel.date < datetime.now())\
        .order_by(StampModel.date)
    today = datetime.now().day
    if not qs.count():
        return []
    days = defaultdict(list)
    for record in qs:
        if today == record.date.day:
            continue
        days[record.date.day].append(record)
    final_values = []
    for day, records in days.items():
        final_values.append(
            {
                'date': datetime.now().date().replace(month=records[0].date.month, day=day),
                'sell': min([x.sell for x in records]),
                'buy': max([x.buy for x in records])
             })
    return final_values
