import requests
from pprint import pprint

CURRENCY = 'US'
NAME = 'Tinkoff Course'

api_url = 'https://api.tinkoff.ru/v1/currency_rates?from=USD&to=RUB'


def parse():
    json_data = requests.get(api_url)
    data = json_data.json()
    for elem in data['payload']['rates']:
        if elem['category'] == 'DebitCardsTransfers':
            return {'buy': elem['buy'], 'sell': elem['sell']}


if __name__ == '__main__':
    print(parse())
