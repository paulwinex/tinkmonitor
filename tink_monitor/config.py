from pathlib import Path
import json

default_config = Path(__file__).parent / 'default_config.json'
config_file = Path.home() / '.tink_monitor.json'


def read():
    conf = {}
    with default_config.open() as f:
        conf.update(json.load(f))
    if config_file.exists():
        with config_file.open() as f:
            conf.update(json.load(f))
    return conf


def save(data):
    with default_config.open() as f:
        default = json.load(f)
    data = {k: v for k, v in data.items() if v != default.get(k)}
    with config_file.open('w') as f:
        json.dump(data, f, indent=2)


def update(data):
    with default_config.open() as f:
        default = json.load(f)
    conf = {}
    if config_file.exists():
        with config_file.open() as f:
            conf = json.load(f)
    data = {k: v for k, v in data.items() if v != default.get(k)}
    conf.update(data)
    with config_file.open('w') as f:
        json.dump(conf, f, indent=2)
