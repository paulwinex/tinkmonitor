import os

from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from datetime import datetime, timedelta
from .ui_files import main_widget_UI
# from .graph import Graph
from .week_widget import WeekWidget
from .utils import set_style
from .config_dialog import ConfigDialog
from ..config import read, save, update
from .. import store


class CourseWidget(QWidget, main_widget_UI.Ui_Tinkus):
    onClose = Signal()
    config_changed = Signal()

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Tool)
        self.data = {
            'snap_size': 10
        }
        self.week = WeekWidget()
        self.table_ly.addWidget(self.week)
        self.close_btn.clicked.connect(self.close)
        self.values = []
        self.isMove = False
        self.isResize = False
        self.currPos = self.startPos = 0
        self.curSize = self.size()
        self.on_init()

    def reload(self, data=None):
        conf = data or read()
        font_size = conf.get('font_size', 10)
        on_top = conf.get('on_top', False)
        geo = conf.get('geo')
        # font
        fnt = self.font()
        fnt.setPixelSize(font_size)
        fnt.setBold(True)
        for lb in (self.curr_sell_lb, self.max_sell_today_lb, self.max_sell_week_lb):
            lb.setFont(fnt)
        self.currensy_lb.setObjectName('title_lb')
        # on top
        if on_top:
            if self.windowFlags() & Qt.WindowStaysOnTopHint == 0:
                self.setWindowFlags(Qt.FramelessWindowHint | Qt.Tool | Qt.WindowStaysOnTopHint)
                self.show()
        else:
            if self.windowFlags() & Qt.WindowStaysOnTopHint > 0:
                self.setWindowFlags(Qt.FramelessWindowHint | Qt.Tool)
                self.show()
        # geo
        if geo:
            self.move(*geo['pos'])
            self.resize(*geo['size'])
        alarm_enabled = any([conf.get('sell_above_alarm'), conf.get('sell_below_alarm'),
                             conf.get('buy_above_alarm'), conf.get('buy_below_alarm')])
        if alarm_enabled:
            self.bell_btn.show()
            if conf.get('allow_alarm', True):
                self.bell_btn.setText('🔔')
            else:
                self.bell_btn.setText('⛔️')
        else:
            self.bell_btn.hide()
        self.reload_data(conf)

    def reload_data(self, conf=None):
        conf = conf or read()
        current = store.get_current_value()
        max_today = store.get_today_max()
        max_week = store.get_week_max()
        # main
        if current:
            self.curr_sell_lb.setText(str(current['buy']))
            self.curr_buy_lb.setText(str(current['sell']))
        if max_today:
            self.max_sell_today_lb.setText(str(max_today['buy']))
            self.max_buy_today_lb.setText(str(max_today['sell']))
        if max_week:
            self.max_sell_week_lb.setText(str(max_week['buy']))
            self.max_buy_week_lb.setText(str(max_week['sell']))
        # week
        week_values = store.get_week_values()
        self.week.reload_week(week_values)

    def _reload_data(self, conf=None):
        conf = conf or read()
        array = get_range_type(conf['graph_range'])
        # self.gr.reload(conf, array)
        import random

        self.week.reload_week(store.get_week_values())
        count = array.count()
        if count:
            rec = array[-1]
            buy_prefix = sell_prefix = ''
            buy_color = sell_color = 'white'
            if count > 1:
                perv_rec = array[-2]
                buy_prefix = '▲' if rec.buy > perv_rec.buy else ('' if rec.buy == perv_rec.buy else '▼')
                sell_prefix = '▲' if rec.sell > perv_rec.sell else ('' if rec.sell == perv_rec.sell else '▼')
                buy_color = 'red' if rec.buy < perv_rec.buy else ('white' if rec.buy == perv_rec.buy else 'green')
                sell_color = 'red' if rec.sell > perv_rec.sell else ('white' if rec.sell == perv_rec.sell else 'green')
            self.buy_lb.setText(f"{buy_prefix}{rec.buy}")
            self.buy_lb.setStyleSheet(f'color: {buy_color}')
            self.sale_lb.setText(f"{sell_prefix}{rec.sell}")
            self.sale_lb.setStyleSheet(f'color: {sell_color}')

    def on_init(self):
        self.close_btn.setText('⨉')
        self.close_btn.setFlat(True)
        for lb in (
                self.curr_sell_lb,
                self.curr_buy_lb,
                self.max_sell_today_lb,
                self.max_buy_today_lb,
                self.max_sell_week_lb,
                self.max_buy_week_lb):
            lb.setText('0')
        for lb in self.title_current, self.title_today, self.title_week:
            lb.setProperty("label1", True)
        self.bell_btn.setFixedSize(QSize(20, 20))
        self.bell_btn.setFlat(True)
        self.bell_btn.setCheckable(True)
        self.bell_btn.clicked.connect(self.bell_pressed)
        if not os.getenv('DEBUG'):
            self.check_alarm_btn.hide()
        set_style(self)

    def open_config_dialog(self):
        dial = ConfigDialog(read(), self)
        dial.on_changed.connect(self.reload)
        if dial.exec():
            conf_data = read()
            data = dial.get_data()
            data['geo'] = dict(
                pos=[self.pos().x(), self.pos().y()],
                size=[self.size().width(), self.size().height()]
            )
            conf_data.update(data)
            save(conf_data)
            self.config_changed.emit()
        else:
            self.reload()

    def closeEvent(self, event: QCloseEvent) -> None:
        self.onClose.emit()
        QApplication.instance().processEvents()
        return super().closeEvent(event)
        # if QMessageBox.question(self, 'Close Widget', 'Exit?', QMessageBox.Yes|QMessageBox.Cancel) == QMessageBox.Yes:
        #     self.onClose.emit()
        #     return super().closeEvent(event)
        # else:
        #     event.ignore()

    def mouse_moved(self, event):
        if self.isMove:
            new = event.globalPos()
            new_pos = new + self.currPos - self.startPos
            self.move(new_pos.x(), new_pos.y())
        elif self.isResize:
            new_pos = event.globalPos() - self.startPos
            new_size = self.curSize + QSize(new_pos.x(), new_pos.y())
            self.resize(new_size.width(), new_size.height())

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.isMove = True
            self.startPos = event.globalPos()
            self.currPos = self.pos()
        elif event.button() == Qt.MiddleButton:
            self.isResize = True
            self.curSize = self.size()
            self.startPos = event.globalPos()
        else:
            self.isMove = False
            self.isResize = False
        QWidget.mousePressEvent(self, event)

    def update_geo(self):
        geo = dict(
            pos=[self.pos().x(), self.pos().y()],
            size=[self.size().width(), self.size().height()]
        )
        update(dict(geo=geo))

    def mouseMoveEvent(self, event):
        self.mouse_moved(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.isResize = False
            self.update_geo()
        if event.button() == Qt.LeftButton:
            self.isMove = False
            self.update_geo()
        if event.button() == Qt.RightButton:
            self.open_config_dialog()
        QWidget.mouseReleaseEvent(self, event)

    def bell_pressed(self):
        data = read()
        data['allow_alarm'] = not self.bell_btn.isChecked()
        save(data)
        self.reload()
