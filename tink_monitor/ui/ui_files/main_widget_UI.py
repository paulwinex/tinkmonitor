# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main_widget.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_Tinkus(object):
    def setupUi(self, Tinkus):
        if not Tinkus.objectName():
            Tinkus.setObjectName(u"Tinkus")
        Tinkus.resize(346, 112)
        self.verticalLayout = QVBoxLayout(Tinkus)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 2)
        self.top_bar = QWidget(Tinkus)
        self.top_bar.setObjectName(u"top_bar")
        self.horizontalLayout_2 = QHBoxLayout(self.top_bar)
        self.horizontalLayout_2.setSpacing(3)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(6, 1, 1, 1)
        self.currensy_lb = QLabel(self.top_bar)
        self.currensy_lb.setObjectName(u"currensy_lb")

        self.horizontalLayout_2.addWidget(self.currensy_lb)

        self.bell_btn = QPushButton(self.top_bar)
        self.bell_btn.setObjectName(u"bell_btn")
        self.bell_btn.setMaximumSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.bell_btn)

        self.check_alarm_btn = QPushButton(self.top_bar)
        self.check_alarm_btn.setObjectName(u"check_alarm_btn")

        self.horizontalLayout_2.addWidget(self.check_alarm_btn)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.close_btn = QPushButton(self.top_bar)
        self.close_btn.setObjectName(u"close_btn")
        self.close_btn.setMaximumSize(QSize(15, 15))

        self.horizontalLayout_2.addWidget(self.close_btn)


        self.verticalLayout.addWidget(self.top_bar)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setHorizontalSpacing(12)
        self.max_buy_week_lb = QLabel(Tinkus)
        self.max_buy_week_lb.setObjectName(u"max_buy_week_lb")
        self.max_buy_week_lb.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.max_buy_week_lb, 2, 5, 1, 1)

        self.horizontalSpacer_5 = QSpacerItem(40, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_5, 1, 0, 1, 1)

        self.title_today = QLabel(Tinkus)
        self.title_today.setObjectName(u"title_today")
        self.title_today.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.title_today, 0, 3, 1, 1)

        self.max_buy_today_lb = QLabel(Tinkus)
        self.max_buy_today_lb.setObjectName(u"max_buy_today_lb")
        self.max_buy_today_lb.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.max_buy_today_lb, 2, 3, 1, 1)

        self.horizontalSpacer_4 = QSpacerItem(40, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_4, 1, 2, 1, 1)

        self.curr_buy_lb = QLabel(Tinkus)
        self.curr_buy_lb.setObjectName(u"curr_buy_lb")
        self.curr_buy_lb.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.curr_buy_lb, 2, 1, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_3, 1, 4, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 1, 6, 1, 1)

        self.max_sell_week_lb = QLabel(Tinkus)
        self.max_sell_week_lb.setObjectName(u"max_sell_week_lb")
        font = QFont()
        font.setPointSize(18)
        font.setBold(True)
        self.max_sell_week_lb.setFont(font)
        self.max_sell_week_lb.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.max_sell_week_lb, 1, 5, 1, 1)

        self.max_sell_today_lb = QLabel(Tinkus)
        self.max_sell_today_lb.setObjectName(u"max_sell_today_lb")
        self.max_sell_today_lb.setFont(font)
        self.max_sell_today_lb.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.max_sell_today_lb, 1, 3, 1, 1)

        self.title_current = QLabel(Tinkus)
        self.title_current.setObjectName(u"title_current")
        self.title_current.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.title_current, 0, 1, 1, 1)

        self.title_week = QLabel(Tinkus)
        self.title_week.setObjectName(u"title_week")
        self.title_week.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.title_week, 0, 5, 1, 1)

        self.curr_sell_lb = QLabel(Tinkus)
        self.curr_sell_lb.setObjectName(u"curr_sell_lb")
        self.curr_sell_lb.setFont(font)
        self.curr_sell_lb.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.curr_sell_lb, 1, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.line = QFrame(Tinkus)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line)

        self.table_ly = QVBoxLayout()
        self.table_ly.setObjectName(u"table_ly")

        self.verticalLayout.addLayout(self.table_ly)

        self.verticalLayout.setStretch(3, 1)

        self.retranslateUi(Tinkus)

        QMetaObject.connectSlotsByName(Tinkus)
    # setupUi

    def retranslateUi(self, Tinkus):
        Tinkus.setWindowTitle(QCoreApplication.translate("Tinkus", u"Form", None))
        self.currensy_lb.setText(QCoreApplication.translate("Tinkus", u"USD", None))
        self.bell_btn.setText(QCoreApplication.translate("Tinkus", u"0", None))
        self.check_alarm_btn.setText(QCoreApplication.translate("Tinkus", u"Check", None))
        self.close_btn.setText(QCoreApplication.translate("Tinkus", u"x", None))
        self.max_buy_week_lb.setText(QCoreApplication.translate("Tinkus", u"12.0", None))
        self.title_today.setText(QCoreApplication.translate("Tinkus", u"today", None))
        self.max_buy_today_lb.setText(QCoreApplication.translate("Tinkus", u"10.0", None))
        self.curr_buy_lb.setText(QCoreApplication.translate("Tinkus", u"9.0", None))
        self.max_sell_week_lb.setText(QCoreApplication.translate("Tinkus", u"12.0", None))
        self.max_sell_today_lb.setText(QCoreApplication.translate("Tinkus", u"12.0", None))
        self.title_current.setText(QCoreApplication.translate("Tinkus", u"current", None))
        self.title_week.setText(QCoreApplication.translate("Tinkus", u"week", None))
        self.curr_sell_lb.setText(QCoreApplication.translate("Tinkus", u"10.0", None))
    # retranslateUi

