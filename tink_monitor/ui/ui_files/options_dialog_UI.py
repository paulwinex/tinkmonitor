# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'options_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QDialog, QDoubleSpinBox,
    QGridLayout, QGroupBox, QLabel, QLineEdit,
    QPushButton, QSizePolicy, QSpinBox, QVBoxLayout,
    QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(361, 277)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBox = QGroupBox(Dialog)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.below_sell_sbx = QDoubleSpinBox(self.groupBox)
        self.below_sell_sbx.setObjectName(u"below_sell_sbx")

        self.gridLayout.addWidget(self.below_sell_sbx, 2, 1, 1, 1)

        self.buy_above_alarm_cbx = QCheckBox(self.groupBox)
        self.buy_above_alarm_cbx.setObjectName(u"buy_above_alarm_cbx")
        self.buy_above_alarm_cbx.setLayoutDirection(Qt.RightToLeft)

        self.gridLayout.addWidget(self.buy_above_alarm_cbx, 1, 2, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 0, 3, 1, 1)

        self.buy_below_alarm_cbx = QCheckBox(self.groupBox)
        self.buy_below_alarm_cbx.setObjectName(u"buy_below_alarm_cbx")
        self.buy_below_alarm_cbx.setLayoutDirection(Qt.RightToLeft)

        self.gridLayout.addWidget(self.buy_below_alarm_cbx, 2, 2, 1, 1)

        self.below_buy_sbx = QDoubleSpinBox(self.groupBox)
        self.below_buy_sbx.setObjectName(u"below_buy_sbx")

        self.gridLayout.addWidget(self.below_buy_sbx, 2, 3, 1, 1)

        self.sell_below_alarm_cbx = QCheckBox(self.groupBox)
        self.sell_below_alarm_cbx.setObjectName(u"sell_below_alarm_cbx")
        self.sell_below_alarm_cbx.setLayoutDirection(Qt.RightToLeft)

        self.gridLayout.addWidget(self.sell_below_alarm_cbx, 2, 0, 1, 1)

        self.sell_above_alarm_cbx = QCheckBox(self.groupBox)
        self.sell_above_alarm_cbx.setObjectName(u"sell_above_alarm_cbx")
        self.sell_above_alarm_cbx.setLayoutDirection(Qt.RightToLeft)

        self.gridLayout.addWidget(self.sell_above_alarm_cbx, 1, 0, 1, 1)

        self.above_buy_sbx = QDoubleSpinBox(self.groupBox)
        self.above_buy_sbx.setObjectName(u"above_buy_sbx")

        self.gridLayout.addWidget(self.above_buy_sbx, 1, 3, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout.addWidget(self.label_4, 0, 1, 1, 1)

        self.above_sell_sbx = QDoubleSpinBox(self.groupBox)
        self.above_sell_sbx.setObjectName(u"above_sell_sbx")

        self.gridLayout.addWidget(self.above_sell_sbx, 1, 1, 1, 1)

        self.tg_bot_token_le = QLineEdit(self.groupBox)
        self.tg_bot_token_le.setObjectName(u"tg_bot_token_le")

        self.gridLayout.addWidget(self.tg_bot_token_le, 3, 1, 1, 3)

        self.tg_chat_id_le = QLineEdit(self.groupBox)
        self.tg_chat_id_le.setObjectName(u"tg_chat_id_le")

        self.gridLayout.addWidget(self.tg_chat_id_le, 4, 1, 1, 3)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 3, 0, 1, 1)

        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_6, 4, 0, 1, 1)

        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 1)
        self.gridLayout.setColumnStretch(3, 1)

        self.verticalLayout_2.addLayout(self.gridLayout)


        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_3 = QGroupBox(Dialog)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_2 = QGridLayout(self.groupBox_3)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.on_top_cbx = QCheckBox(self.groupBox_3)
        self.on_top_cbx.setObjectName(u"on_top_cbx")

        self.gridLayout_2.addWidget(self.on_top_cbx, 0, 1, 1, 1)

        self.font_size_sbx = QSpinBox(self.groupBox_3)
        self.font_size_sbx.setObjectName(u"font_size_sbx")
        self.font_size_sbx.setMinimum(8)
        self.font_size_sbx.setValue(16)

        self.gridLayout_2.addWidget(self.font_size_sbx, 1, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox_3)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(100, 0))
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)

        self.label_5 = QLabel(self.groupBox_3)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(100, 0))
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_5, 1, 0, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_3)

        self.save_btn = QPushButton(Dialog)
        self.save_btn.setObjectName(u"save_btn")

        self.verticalLayout.addWidget(self.save_btn)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Options", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Alarm", None))
        self.buy_above_alarm_cbx.setText("")
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Buy", None))
        self.buy_below_alarm_cbx.setText("")
        self.sell_below_alarm_cbx.setText(QCoreApplication.translate("Dialog", u"Below", None))
        self.sell_above_alarm_cbx.setText(QCoreApplication.translate("Dialog", u"Above", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Sell", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"TG Bot Token", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"TG Chat ID", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Dialog", u"Display", None))
        self.on_top_cbx.setText("")
        self.label_3.setText(QCoreApplication.translate("Dialog", u"On Top", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Font Size", None))
        self.save_btn.setText(QCoreApplication.translate("Dialog", u"Save", None))
    # retranslateUi

