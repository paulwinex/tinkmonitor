from pathlib import Path


style_file = Path(__file__).parent / 'style.css'
if style_file.exists():
    style = style_file.read_text()
else:
    style = ''


def set_style(widget):
    widget.setStyleSheet(style)
