from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from .ui_files import options_dialog_UI
from .utils import set_style


class ConfigDialog(QDialog, options_dialog_UI.Ui_Dialog):
    on_changed = Signal(object)

    def __init__(self, init_data, parent):
        super().__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.Tool)
        set_style(self)
        self.font_size_sbx.valueChanged.connect(self.interactive_update)
        self.on_top_cbx.clicked.connect(self.interactive_update)
        self.sell_above_alarm_cbx.clicked.connect(self.interactive_update)
        self.sell_below_alarm_cbx.clicked.connect(self.interactive_update)
        self.buy_above_alarm_cbx.clicked.connect(self.interactive_update)
        self.buy_below_alarm_cbx.clicked.connect(self.interactive_update)

        self.save_btn.clicked.connect(self.accept)
        self.init(init_data)

    def init(self, data):
        self.sell_above_alarm_cbx.setChecked(data.get('sell_above_alarm', False))
        self.sell_below_alarm_cbx.setChecked(data.get('sell_below_alarm', False))
        self.buy_above_alarm_cbx.setChecked(data.get('buy_above_alarm', False))
        self.buy_below_alarm_cbx.setChecked(data.get('buy_below_alarm', False))

        self.above_buy_sbx.setValue(data.get('above_buy', 0))
        self.below_buy_sbx.setValue(data.get('below_buy', 0))

        self.above_sell_sbx.setValue(data.get('above_sell', 0))
        self.below_sell_sbx.setValue(data.get('below_sell', 0))

        self.tg_bot_token_le.setText(data.get('tg_token', ''))
        self.tg_chat_id_le.setText(data.get('chat_id', ''))

        self.font_size_sbx.setValue(data.get('font_size', 25))
        self.on_top_cbx.setChecked(data.get('on_top', False))

    def get_data(self):
        return dict(
            sell_above_alarm=self.sell_above_alarm_cbx.isChecked(),
            sell_below_alarm=self.sell_below_alarm_cbx.isChecked(),
            buy_above_alarm=self.buy_above_alarm_cbx.isChecked(),
            buy_below_alarm=self.buy_below_alarm_cbx.isChecked(),

            above_buy=self.above_buy_sbx.value(),
            below_buy=self.below_buy_sbx.value(),
            above_sell=self.above_sell_sbx.value(),
            below_sell=self.below_sell_sbx.value(),

            tg_token=self.tg_bot_token_le.text(),
            chat_id=self.tg_chat_id_le.text(),

            on_top=self.on_top_cbx.isChecked(),
            font_size=self.font_size_sbx.value(),
        )

    def interactive_update(self, *args):
        self.on_changed.emit(self.get_data())
