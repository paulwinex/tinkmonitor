from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from datetime import datetime


class WeekWidget(QWidget):
    def __init__(self):
        super().__init__()
        ly = QHBoxLayout(self)
        ly.setContentsMargins(2, 2, 2, 2)
        self.boxes = []
        for i in range(7):
            day = DayWidget()
            self.boxes.append(day)
            ly.addWidget(day)
            if i < 6:
                line = QFrame()
                line.setFrameShape(QFrame.VLine)
                line.setFrameShadow(QFrame.Sunken)
                ly.addWidget(line)

    def reload_week(self, week: list):
        empty = [None] * 7
        week = empty[:-len(week)] + week
        for i in range(len(week)):
            if week[i]:
                self.boxes[i].set_data(**week[i])
            else:
                self.boxes[i].reset()


class DayWidget(QWidget):
    def __init__(self):
        super().__init__()
        ly = QVBoxLayout(self)
        ly.setContentsMargins(0, 0, 0, 0)
        self.date_lb = QLabel('-----', alignment=Qt.AlignHCenter)
        self.date_lb.setProperty('is_date', True)
        ly.addWidget(self.date_lb)
        self.value_lb = QLabel('-----', alignment=Qt.AlignHCenter)
        val_font: QFont = self.value_lb.font()
        val_font.setBold(True)
        self.value_lb.setFont(val_font)
        ly.addWidget(self.value_lb)

    def set_data(self, date: datetime, buy: float, **kwargs):
        self.date_lb.setText(date.strftime('%m.%d'))
        self.value_lb.setText(str(round(buy, 2)))

    def reset(self):
        self.date_lb.setText('-----')
        self.value_lb.setText('-----')
